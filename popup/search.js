document.addEventListener('DOMContentLoaded', function () {
    const searchForm = document.getElementById('searchForm');
    const searchInput = document.querySelector('input[type="text"]');

    function redirectToSearch(event) {
        // Empêcher le formulaire de se soumettre de la manière standard
        event.preventDefault();

        const searchTerm = searchInput.value;
        const searchUrl = `https://framalibre.org/recherche-par-crit-res?keys=${encodeURIComponent(searchTerm)}`;

        // Rediriger vers la page de recherche
        window.open(searchUrl, '_blank');
    }

    // Ajouter un écouteur d'événement pour le formulaire
    searchForm.addEventListener('submit', redirectToSearch);
});
