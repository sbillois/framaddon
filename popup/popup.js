// Faire une boucle pour afficher les lien avec le json
let data;

// Fonction pour charger les données de la popup
function loadPopupData(callback) {
    // Essayez d'obtenir les données du stockage local
    browser.storage.local.get('popupData', function (result) {
        if (result.popupData) {
            callback(result.popupData); // Utilisez les données du stockage local
        } else {
            // Si aucune donnée n'est trouvée dans le stockage local, chargez les données par défaut de popup/popup.json
            fetch('./popup.json')
                .then(response => response.json())
                .then(data => {
                    callback(data);
                })
                .catch(error => {
                    console.error('Erreur lors du chargement de popup/popup.json:', error);
                });
        }
    });
}

// Utilisez la fonction loadPopupData pour charger les données lorsque la popup est ouverte
loadPopupData(function (data) {
    // Ici, "data" contient les données que vous devez utiliser pour remplir votre popup
    // Générer le HTML à partir des données
    generateHTMLFromJSON(data);
});




function generateHTMLFromJSON(data) {
    const container = document.querySelector('.allBox');

    for (const key in data) {
        const title = document.createElement('h2');
        title.textContent = key;
        container.appendChild(title);

        const box = document.createElement('div');
        box.className = 'box';
        box.id = key.toLowerCase();

        const itemsContainer = document.createElement('div');
        itemsContainer.className = 'allItems';

        data[key].forEach(item => {
            const itemDiv = document.createElement('div');
            itemDiv.className = 'item';

            const anchor = document.createElement('a');
            anchor.href = item.link;

            const img = document.createElement('img');
            if (item.icon != null && item.icon != undefined && item.icon != "" && item.icon != " ") {
                img.src = item.icon;
            } else {
                img.src = '../icons/framasoft-96.png';
            }

            if (item.alt !== null && item.alt !== undefined && item.alt !== "" && item.alt !== " ") {
                img.alt = item.alt;
            } else {
                img.alt = 'Icone par défaut';
            }

            anchor.appendChild(img);

            const paragraph = document.createElement('p');
            const innerAnchor = document.createElement('a');
            innerAnchor.href = item.link;
            innerAnchor.textContent = item.name;
            paragraph.appendChild(innerAnchor);
            anchor.appendChild(paragraph);

            itemDiv.appendChild(anchor);
            itemsContainer.appendChild(itemDiv);
        });

        box.appendChild(itemsContainer);
        container.appendChild(box);
    }
}


generateHTMLFromJSON(data);
