document.addEventListener('DOMContentLoaded', function () {
    const jsonContentEl = document.getElementById('jsonContent');
    const saveBtn = document.getElementById('saveBtn');
    const restoreBtn = document.getElementById('restoreBtn'); // Utiliser le bouton déjà présent dans le HTML

    // Fonction pour charger les données par défaut
    function loadDefaultData() {
        fetch('../popup/popup.json')
            .then(response => response.json())
            .then(data => {
                jsonContentEl.value = JSON.stringify(data, null, 4);
            })
            .catch(error => {
                console.error('Erreur lors du chargement de popup/popup.json:', error);
            });
    }

    // Charger les données du stockage local ou les données par défaut si le stockage local est vide
    browser.storage.local.get('popupData', function (result) {
        if (result.popupData) {
            jsonContentEl.value = JSON.stringify(result.popupData, null, 4);
        } else {
            loadDefaultData();
        }
    });

    // Sauvegarder les modifications
    saveBtn.addEventListener('click', function () {
        try {
            const jsonData = JSON.parse(jsonContentEl.value);
            browser.storage.local.set({ popupData: jsonData });
            alert('Données sauvegardées avec succès !');
        } catch (error) {
            alert('Erreur lors de la sauvegarde : ' + error.message);
        }
    });

    // Restaurer les données par défaut
    restoreBtn.addEventListener('click', function () {
        if (confirm("Êtes-vous sûr de vouloir restaurer les données par défaut ?")) {
            browser.storage.local.remove('popupData', function () {
                loadDefaultData();
                //alert('Données restaurées aux valeurs par défaut !');
            });
        }
    });
});
