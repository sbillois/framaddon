// Fonction pour gérer la recherche dans le BO
function handleSearchRequest(info, tab) {
    const baseUrl = "https://framalibre.org/recherche-par-crit-res";
    const searchText = info.selectionText;

    // Créez l'URL avec le texte de recherche
    const url = `${baseUrl}?keys=${encodeURIComponent(searchText)}`;

    // Ouvrez un nouvel onglet avec l'URL
    browser.tabs.create({ url: url });
}

// Créer des options de menu contextuel lors de la sélection de texte
browser.contextMenus.create({
    id: "search-framalibre",
    title: "Rechercher dans l'annuaire Framalibre",
    contexts: ["selection"]
});

// Ajout d'un écouteur pour gérer les clics sur les options du menu contextuel
browser.contextMenus.onClicked.addListener(handleSearchRequest);
